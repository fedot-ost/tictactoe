'use strict';
var cfg = require('../srv_cfg.js');
console.log(cfg);
//npm modules
let http = require('http'),
    fs = require('fs'),
    ws = require('ws');
let state = {'test':'123'};

console.log('starting server');

class Server{
    constructor(){
        this.createServer();
        this.setSocketConnection();
    console.log('server done');
    }
    createServer(){
        let self = this;
        this.server = new http.createServer(
            function(request,response){
                //self.request = request;
                //self.response = response;
                self.fileSender(request, response);
            }
        );
        this.server.listen(cfg.HTTP_PORT,cfg.HOST);
    }
    fileSender(request, response){
        //if(this.request.url.indexOf('.css') != -1){
        if(/\.css/.test(request.url)){
            let path = '.'+request.url.split('?')[0];
            console.log('PRE:  ' + request.url);
            fs.readFile(path, function (err, content) {
                if(!err){
                    console.log('POST: ' + request.url);
                    response.writeHead(200, {'Content-Type': 'text/css'});
                    response.write(content);
                    response.end();
                }else{
                    response.statusCode = 404;
                    response.end();
                }
            });
        //скрипты
        }else if(/\.js/.test(request.url)){
            let path = '.'+request.url.split('?')[0];
            console.log('PRE:  ' + request.url);
            fs.readFile(path, function (err, content) {
                if(!err){
                    console.log('POST: ' + request.url);
                    response.writeHead(200, {'Content-Type': 'text/javascript'});
                    response.write(content);
                    response.end();
                }else{
                    response.statusCode = 404;
                    response.end();
                }
            });
        //если html-страница
        }else if(!!cfg.PAGES[request.url]){
            let path = './'+cfg.PAGES[request.url];
            
            fs.readFile(path,function(err,content){
                if(!err){
                    response.statusCode=200;
                    response.write(content);
                    response.end();
                }else{
                    response.statusCode = 404;
                    response.end();
                }
            })
        }
    }
    setSocketConnection(){
        let currentConnection;
        this.socket = new ws.Server({
            port: cfg.WS_PORT
        });
        this.socket.on('connection',function(currentConnection){
            currentConnection.send(JSON.stringify(state));
        });
    }
}
let tictacoe_server = new Server();
