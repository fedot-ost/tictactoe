module.exports = {
    /* general */
    debug: true,

    /* canvas */
    canvas: {
        width: 600,
        height: 600,
        padding: 10,
        background: 'ffffff',

        /* cross, circle */
        //lineColor: '#000000',
        lineColor: 'green',
        lineWidth: 15,
        lineCap: 'round',
        cellPadding: 10,

        /* grid */
        gridColor: '#888888',
        gridWidth: 5,
        gridCap: 'round'
    }
}
