export default class Canvas{
    constructor(node, cfg){
        this.node = node;
        this.cfg = cfg;
        this.ctx = node.getContext('2d');
        //тк эти ведичины исп-ся в нескольких методах
        this.cfg.size = 3;
        this.cfg.cell_w = (this.cfg.width - this.cfg.gridWidth * 2 - this.cfg.padding * 2) / this.cfg.size; //  this.cfg.size - исп-ся в неск. методах
        this.cfg.cell_h = (this.cfg.height - this.cfg.gridWidth * 2 - this.cfg.padding * 2) / this.cfg.size;
    }
    /* сброс канваса */
    reset(){
        this.ctx.fillStyle = `#${this.cfg.background}`;
        this.ctx.fillRect(0, 0, this.node.width, this.node.height);
    }
    /* отрисовка сетки */
    drawGrid(){
        var ctx = this.ctx,
            cfg = this.cfg,
            pad = cfg.padding,
            cell_w = this.cfg.cell_w,
            cell_h = this.cfg.cell_h,
            line_pad = cfg.gridWidth / 2;

        ctx.lineWidth = cfg.gridWidth;
        ctx.lineCap = cfg.gridCap;
        ctx.strokeStyle = cfg.gridColor;

        ctx.beginPath();
        //L-1
        ctx.moveTo(pad + cell_w + line_pad, pad);
        ctx.lineTo(pad + cell_w + line_pad, cfg.height - pad);
        //L-2
        ctx.moveTo(pad + cell_w * 2 + line_pad * 3, pad);
        ctx.lineTo(pad + cell_w * 2 + line_pad * 3, cfg.height - pad);
        //C-1
        ctx.moveTo(pad, pad + cell_h + line_pad);
        ctx.lineTo(cfg.width - pad, pad + cell_h + line_pad);
        //C-2
        ctx.moveTo(pad, pad + cell_h * 2 + line_pad * 3);
        ctx.lineTo(cfg.width - pad, pad + cell_h * 2 + line_pad * 3);
        // последний переход чтобы правильно отрисовались окончания линий
        ctx.moveTo(-100, -100);
        ctx.closePath();
        ctx.stroke();
    }
    /* отрисовка крестика в заданной клетке */
    drawCross(n){
        let
            pos = this.getPos(n),
            cords = {
                x1: this.cfg.padding + pos.x*(this.cfg.gridWidth + this.cfg.cell_w) + this.cfg.cellPadding/2,
                x2: this.cfg.padding + pos.x*(this.cfg.gridWidth + this.cfg.cell_w) + this.cfg.cell_w - this.cfg.cellPadding/2,
                y1: this.cfg.padding + pos.y*(this.cfg.gridWidth + this.cfg.cell_h) + this.cfg.cellPadding/2,
                y2: this.cfg.padding + pos.y*(this.cfg.gridWidth + this.cfg.cell_h) + this.cfg.cell_h - this.cfg.cellPadding/2
            };
        this.ctx.beginPath();
        this.ctx.strokeStyle = this.cfg.lineColor;
        this.ctx.moveTo(cords.x1,cords.y1);
        this.ctx.lineTo(cords.x2,cords.y2);
        this.ctx.moveTo(cords.x1,cords.y2);
        this.ctx.lineTo(cords.x2,cords.y1);
        this.ctx.closePath();
        this.ctx.stroke();
    }
    /* отрисовка круга в заданной клетке */
    drawCircle(n){
        let
            pos = this.getPos(n),
            center = {
                x: this.cfg.padding+pos.x*(this.cfg.gridWidth+this.cfg.cell_w) + this.cfg.cell_w/2,
                y: this.cfg.padding+pos.y*(this.cfg.gridWidth+this.cfg.cell_h) + this.cfg.cell_h/2
            },
            radius = (this.cfg.cell_w-this.cfg.cellPadding)/2;
        this.ctx.beginPath();
        this.ctx.strokeStyle = this.cfg.lineColor;
        this.ctx.arc(center.x,center.y,radius,0,Math.PI*2,true);
        this.ctx.closePath();
        this.ctx.stroke();
    }
    clearSector(){
        //clear
    }
    getPos(n){
        let
            line = Math.round((n - 1) / this.cfg.size),
            col = n-line*this.cfg.size,
            pos = {
                x: col,
                y: line
            }
        return pos;
    }
}
