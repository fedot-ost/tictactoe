import './styles.css';
import Canvas from './canvas.js';
import config from './config.js';
import server from '../srv_cfg.js';

var qSel = window.document.querySelector;
    
class Tic {
    constructor(body) {
        this.state = {};
        this.dom = {
            body: body
        };
        console.log(this.dom);
        Tic.log('Tic log test', {color: 'green', size: '45'});
        this.init();
    }
    init(){
        this.buildDOM();
        this.openSocket();
        this.setCanvas();
    }
    buildDOM(){
        this.dom.wrap = document.createElement('div');
        this.dom.wrap.classList.add('b-main-wrap');
        
        this.dom.body.appendChild(this.dom.wrap);
    }
    openSocket(){
        this.socket = new WebSocket(`ws://${server.HOST}:${server.WS_PORT}/`);
        this.socket.onopen = (event) => {
            Tic.log('Socket connection established');
        }
        this.socket.onmessage = (event) => {
            Tic.log('Receieved message from socket:');
            Tic.log(event);
        }
        this.socket.onerror = (event) => {
            Tic.log('Error on socket:');
            Tic.log(event);
        }
        this.socket.onclose = (event) => {
            Tic.log('Socket connection closed');
        }
    }
    setCanvas(){
        var cfg = config.canvas;
        
        this.dom.canvas = document.createElement('canvas');
        this.dom.canvas.width = cfg.width;
        this.dom.canvas.height = cfg.height;
        this.dom.canvas.classList.add('b-game__canvas');
        this.dom.wrap.appendChild(this.dom.canvas);
        
        this.cvs = new Canvas(this.dom.canvas, cfg);
        this.cvs.reset();
        this.cvs.drawGrid();
        this.cvs.drawCircle(2);
        this.cvs.drawCross(4);
    }
    /* UTILS */
    static log(msg, options={}){
        var cfg = {
            color: 'navy',
            size: 15,
            weight: 700
        }
        if (config.debug) {
            Object.assign(cfg, options);
            if (typeof msg === 'string') {
                console.log('%c' + msg, 
                    `font-size:${cfg.size}px;` +
                    `color:${cfg.color};` +
                    `font-weight:${cfg.weight};`);
            } else {
                console.log(msg);
            }
        }
    }
}

window.onload = function() {
    var game = new Tic(window.document.body);
}

/*[09.09.2016 22:48:21] Yevgeniy Fedyushkin: 172.16.11.70
[09.09.2016 23:18:07] Yevgeniy Fedyushkin: var socket = new WebSocket("ws://localhost:8081/");
[09.09.2016 23:18:14] Yevgeniy Fedyushkin: socket.send("Привет");
[09.09.2016 23:18:55] Yevgeniy Fedyushkin: socket.onmessage = function(event) {
  console.log("Получены данные " + event.data);
};*/
