'use strict';
const PLUGINS = {
    HTML: require('html-webpack-plugin'),
    EXTRACT_TEXT: require('extract-text-webpack-plugin'),
    COPY_PLUGIN: require('copy-webpack-plugin')
};
const ENTRY_POINTS = {
    client: './source/client.js'
};
const PRODUCTION_DIR = '/prod';
const WATCH_MODE = false;
const HTML_PAGES = {
    INDEX: new PLUGINS.HTML({
    	template: 'source/index.html',
        filename: 'index.html',
        hash: true,
        inject: 'head'
	})
}
const COPY_FILES = {
    SERVER: new PLUGINS.COPY_PLUGIN([{from: 'source/server.js', to: 'server.js'}])
}
const STYLES = {
    MAIN: new PLUGINS.EXTRACT_TEXT('style.css')
}
const LOADERS = {
    JS: {
            test: /\.js$/i,
            loader: 'babel',
            query: {
                presets: [ 'es2015' ],
                plugins: [
                    ['transform-es2015-classes', {
                        'loose': true
                    }]
                ],
		compact: false
            }
        },
    CSS: {
        test: /\.css/i,
        loader: PLUGINS.EXTRACT_TEXT.extract('style-loader', 'css-loader')
    }
}
module.exports = {
    entry: ENTRY_POINTS,
    output: {
        path: __dirname + PRODUCTION_DIR,
        filename: '[name].js'
    },
    plugins : [
        HTML_PAGES.INDEX,
        STYLES.MAIN,
	    COPY_FILES.SERVER
    ],
    module: {
        loaders: [LOADERS.JS,LOADERS.CSS]
    },
    watch: WATCH_MODE
}
